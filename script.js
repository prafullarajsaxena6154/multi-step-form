(document.querySelector('#one').classList.toggle('selected-page'));
let plan = '';
let toggle = false;
let addOnArray = [];
let planPrice = 0;
let addOnPrice=[];
document.addEventListener('click', function (event) {
    if (event.target.id == 'page2Button' || event.target.id == 'change') {
        if( event.target.id == 'change'){
            document.querySelector('#one').classList.toggle('selected-page');
            document.querySelector('#four').classList.toggle('selected-page');
        }
        document.querySelector('#page4').style.display = 'none';
        let name = document.querySelector('#Name').value;
        let email = document.querySelector('#email').value;
        let phone = document.querySelector('#pNum').value;
        if (name.length == 0 || !(/^[A-Za-z\s]*$/).test(name)) {
            document.querySelector('#errorName').style.display = 'flex';
            check = 0;
        }
        else if (name.length != 0 || (/^[A-Za-z\s]*$/).test(name)) {
            document.querySelector('#errorName').style.display = 'none';
            check = 1;
        }
        if (email.length == 0 || !(email.includes('@'))) {
            document.querySelector('#errorEMail').style.display = 'flex';
            check = 0;
        }
        else if (email.length != 0 || (email.includes('@'))) {
            document.querySelector('#errorEMail').style.display = 'none';
            check = 1;
        }
        if (phone.length != 10 || !(/^[0-9]+$/).test(phone)) {
            document.querySelector('#errorPNum').style.display = 'flex';
            check = 0;
        }
        else if (phone.length == 10 || (/^[0-9]+$/).test(phone)) {
            document.querySelector('#errorPNum').style.display = 'none';
            check = 1;
        }
        if ((phone.length == 10 && (/^[0-9]+$/).test(phone)) && email.length != 0 && (email.includes('@')) && name.length != 0 && (/^[A-Za-z\s]*$/).test(name)) {
            document.querySelector('#page1').style.display = 'none';
            document.querySelector('#page2').style.display = 'flex';
            document.querySelector('#one').classList.toggle('selected-page');
            document.querySelector('#two').classList.toggle('selected-page');
        }

    }
    if (event.target.classList.contains('plan-box')) {
        document.querySelectorAll('.plan-box').forEach((ele) => {
            (ele.classList.remove('selected-box'));
        })
        event.target.classList.toggle('selected-box');
        plan = event.target.id;

    }
    if (event.target.parentElement.parentElement.classList.contains('plan-box')) {
        document.querySelectorAll('.plan-box').forEach((ele) => {
            (ele.classList.remove('selected-box'));
        })
        event.target.parentElement.parentElement.classList.toggle('selected-box');
        plan = event.target.parentElement.parentElement.id;
    }

    if (event.target.id == 't1') {
        if (event.target.checked) {
            toggle = true;
            document.querySelector('#toggle1').style.color = 'gray';
            document.querySelector('#toggle2').style.color = '#04285A';
            document.querySelector('#arcadePrice').textContent = '$90/yr';
            document.querySelector('#advancedPrice').textContent = '$120/yr';
            document.querySelector('#proPrice').textContent = '$150/yr';
            document.querySelectorAll('.freeMonth').forEach((ele) => {
                ele.style.display = 'flex';
            })
        }
        else if (!event.target.checked) {
            toggle = false;
            document.querySelector('#toggle2').style.color = 'gray';
            document.querySelector('#toggle1').style.color = '#04285A';
            document.querySelector('#arcadePrice').textContent = '$9/mo';
            document.querySelector('#advancedPrice').textContent = '$12/mo';
            document.querySelector('#proPrice').textContent = '$15/mo';
            document.querySelectorAll('.freeMonth').forEach((ele) => {
                ele.style.display = 'none';
            })
        }
    }
    if (event.target.id == 'backFromStep2') {
        document.querySelector('#page1').style.display = 'flex';
        document.querySelector('#page2').style.display = 'none';
        document.querySelector('#one').classList.toggle('selected-page');
        document.querySelector('#two').classList.toggle('selected-page');
    }
    if (event.target.id == 'page3Button' && plan != '') {
        document.querySelector('#page2').style.display = 'none';
        document.querySelector('#page3').style.display = 'flex';
        document.querySelector('#two').classList.toggle('selected-page');
        document.querySelector('#three').classList.toggle('selected-page');
        if (toggle) {
            document.querySelector('#online-price').textContent = '+$10/yr';
            document.querySelector('#storage-price').textContent = '+$20/yr'
            document.querySelector('#profit-price').textContent = '+$20/yr'
        }
        else if (!toggle) {
            document.querySelector('#online-price').textContent = '+$1/mo';
            document.querySelector('#storage-price').textContent = '+$2/mo'
            document.querySelector('#profit-price').textContent = '+2/mo'
        }
    }
    if (event.target.id == 'backFromStep3') {
        document.querySelector('#page2').style.display = 'flex';
        document.querySelector('#page3').style.display = 'none';
        document.querySelector('#two').classList.toggle('selected-page');
        document.querySelector('#three').classList.toggle('selected-page');
    }
    console.log(event.target);
    if (event.target.classList.contains('addon-box')) {

        document.querySelector(`#${event.target.id}`).classList.toggle('selected-box');
        if (document.querySelector(`#${event.target.id}`).children[0].children[0].checked) {
            document.querySelector(`#${event.target.id}`).children[0].children[0].checked = false;
        }
        else if (!document.querySelector(`#${event.target.id}`).children[0].children[0].checked) {
            document.querySelector(`#${event.target.id}`).children[0].children[0].checked = true;
        }
    }
    if (event.target.parentElement.classList.contains('addon-box')) {

        
        document.querySelector(`#${event.target.parentElement.id}`).classList.toggle('selected-box');
        if (document.querySelector(`#${event.target.parentElement.id}`).children[0].children[0].checked) {
            document.querySelector(`#${event.target.parentElement.id}`).children[0].children[0].checked = false;
        }
        else if (!document.querySelector(`#${event.target.parentElement.id}`).children[0].children[0].checked) {
            document.querySelector(`#${event.target.parentElement.id}`).children[0].children[0].checked = true;
        }

    }
    if (event.target.parentElement.parentElement.classList.contains('addon-box')) {
        
        document.querySelector(`#${event.target.parentElement.parentElement.id}`).classList.toggle('selected-box')
        if (document.querySelector(`#${event.target.parentElement.parentElement.id}`).children[0].children[0].checked) {
            document.querySelector(`#${event.target.parentElement.parentElement.id}`).children[0].children[0].checked = false;
        }
        else if (!document.querySelector(`#${event.target.parentElement.parentElement.id}`).children[0].children[0].checked) {
            document.querySelector(`#${event.target.parentElement.parentElement.id}`).children[0].children[0].checked = true;
        }
    }
    if (event.target.id == 'page4Button') {
        document.querySelector('#page3').style.display = 'none';
        document.querySelector('#page4').style.display = 'flex';
        document.querySelector('#three').classList.toggle('selected-page');
        document.querySelector('#four').classList.toggle('selected-page');
        addOnArray = [];
        addOnPrice = [];
        document.querySelector('.add-ons-summary').innerHTML = '';
        document.querySelectorAll('.add-ons').forEach((ele) => {
            if (ele.checked) {
                addOnArray.push(ele.parentElement.parentElement.id);
            }
        });
        if (toggle) {
            document.querySelector('#plan').innerHTML = `${plan} (Yearly)`;
            if (plan == 'Arcade') {
                document.querySelector('#plan-price').innerHTML = `$90/yr`;
                planPrice = 90;
            }
            else if (plan == 'Advanced') {
                document.querySelector('#plan-price').innerHTML = `$120/yr`;
                planPrice = 120;
            }
            else if (plan == 'Pro') {
                document.querySelector('#plan-price').innerHTML = `$150/yr`;
                planPrice = 150;
            }
        }
        if (!toggle) {
            document.querySelector('#plan').innerHTML = `${plan} (Monthly)`;
            if (plan == 'Arcade') {
                document.querySelector('#plan-price').innerHTML = `$9/mo`;
                planPrice = 9;
            }
            else if (plan == 'Advanced') {
                document.querySelector('#plan-price').innerHTML = `$12/mo`;
                planPrice = 12;
            }
            else if (plan == 'Pro') {
                document.querySelector('#plan-price').innerHTML = `$15/mo`;
                planPrice = 15;
            }
        }
        if (addOnArray.length > 0) {
            let line = document.createElement('div');
            line.innerHTML = `<hr>`;
            document.querySelector('.add-ons-summary').append(line);
            addOnArray.forEach((ele) => {
                let child = document.createElement('div');
                child.setAttribute(`class`, `selected-addons`);
                if (toggle) {
                    if (ele == 'online-addon') {
                        addOnPrice.push(10);
                        child.innerHTML = `<div>Online Service</div> 
                    <div>+$10/yr</div>`
                    }
                    else if (ele == 'storage-addon') {
                        addOnPrice.push(20);
                        child.innerHTML = `<div>Larger Storage</div> 
                    <div>+$20/yr</div>`
                    }
                    else if (ele == 'customizable-addon') {
                        addOnPrice.push(20);
                        child.innerHTML = `<div>Customizable Profile</div> 
                    <div>+$20/yr</div>`
                    }
                }
                else if (!toggle) {
                    if (ele == 'online-addon') {
                        addOnPrice.push(1);
                        child.innerHTML = `<div>Online Service</div> 
                    <div>+$1/mo</div>`
                    }
                    else if (ele == 'storage-addon') {
                        addOnPrice.push(2);
                        child.innerHTML = `<div>Larger Storage</div> 
                    <div>+$2/mo</div>`
                    }
                    else if (ele == 'customizable-addon') {
                        addOnPrice.push(2);
                        child.innerHTML = `<div>Customizable Profile</div> 
                    <div>+$2/mo</div>`
                    }
                }
                document.querySelector('.add-ons-summary').append(child);
            })

        }
        let total = planPrice + addOnPrice.reduce((acc, ele) =>{
            acc+=ele;
            return acc;
        },0);
        document.getElementById('final-price').innerHTML = toggle ? `$${total}/yr`: `$${total}/mo`;
    }
    if (event.target.id == 'backFromStep4') {
        document.querySelector('#page3').style.display = 'flex';
        document.querySelector('#page4').style.display = 'none';
        document.querySelector('#three').classList.toggle('selected-page');
        document.querySelector('#four').classList.toggle('selected-page');
    }
    if (event.target.id == 'page5Button') {
        document.querySelector('#page4').style.display = 'none';
        document.querySelector('#page5').style.display = 'flex';
    }

})